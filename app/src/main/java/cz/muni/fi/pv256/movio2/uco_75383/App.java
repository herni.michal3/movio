package cz.muni.fi.pv256.movio2.uco_75383;

import android.app.Application;
import android.os.StrictMode;

public class App extends Application{
    @Override
    public void onCreate() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().detectAll().build();
        StrictMode.setThreadPolicy(policy);
        StrictMode.VmPolicy vmPolicy = new StrictMode.VmPolicy.Builder().detectAll().build();
        StrictMode.setVmPolicy(vmPolicy);
        super.onCreate();
    }
}
